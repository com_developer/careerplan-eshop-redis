package com.ruyuan.careerplan.goodscart.constants;

/**
 * 分布式锁KEY
 * @author zhonghuashishan
 */
public class RedisLockKeyConstants {

    /**
     * 购物车数据持久化
     */
    public static final String SHOPPING_CART_PERSISTENCE_KEY = "SHOPPING_CART_PERSISTENCE_KEY:";

}
